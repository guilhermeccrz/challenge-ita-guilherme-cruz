# Challenge Itaú [1,2,3] - Guilherme Cruz

   - [Challenge#1](https://gitlab.com/guilhermeccrz/challenge-ita-guilherme-cruz/-/tree/master/challenge-01) 
   - [Challenge#2](https://gitlab.com/guilhermeccrz/challenge-ita-guilherme-cruz/-/tree/master/challenge-02)
   - [Challenge#3](https://gitlab.com/guilhermeccrz/challenge-ita-guilherme-cruz/-/tree/master/challenge-03)
   

## License
[MIT](https://choosealicense.com/licenses/mit/)